package com.example.demo_test_jwt.repository;

import com.example.demo_test_jwt.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

@Mapper
public interface UserRepository {
    @Select("SELECT * FROM user_tb WHERE user_email = #{username}")
    @Result(property = "userId", column = "user_id")
    @Result(property = "email", column = "user_email")
    @Result(property = "password", column = "user_password")
    UserDetails getUserByEmail(String username);

    @Insert("INSERT INTO user_tb(user_email, user_password) VALUES(#{user.email}, #{user.password})")
    void register(@Param("user") User user);

//    @Select("SELECT * FROM user_tb WHERE user_email = #{email}")
//    @Result(property = "userId", column = "user_id")
//    @Result(property = "email", column = "user_email")
//    @Result(property = "password", column = "user_password")
//    User findByEmail(String email);
}
