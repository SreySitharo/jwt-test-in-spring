package com.example.demo_test_jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTestJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoTestJwtApplication.class, args);
    }

}
